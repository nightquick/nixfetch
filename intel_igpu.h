#ifndef INTEL_IGPU_H
#define INTEL_IGPU_H

/**
 * iGPU series: Older Intel iGPUs
*/
#define IGPU_830M                   0x3577
#define IGPU_845G                   0x2562
#define IGPU_852GM                  0x3582
#define IGPU_855GM                  0x3582
#define IGPU_865G                   0x2572
#define IGPU_915G                   0x2582
#define IGPU_E7221G                 0x258A
#define IGPU_915GM                  0x2592
#define IGPU_945G                   0x2772
#define IGPU_945GM                  0x27A2
#define IGPU_945GME                 0x27AE
#define IGPU_Q35                    0x29B2
#define IGPU_G33                    0x29C2
#define IGPU_Q33                    0x29D2
#define IGPU_PINEVIEW_M             0xA011
#define IGPU_PINEVIEW               0xA001
#define IGPU_965GME                 0x2A12
#define IGPU_GLE                    0x2A12
#define IGPU_965GM                  0x2A02
#define IGPU_946GZ                  0x2972
#define IGPU_965Q                   0x2992
#define IGPU_965G_v0                0x29A2
#define IGPU_965G_v1                0x2982
#define IGPU_B43_v0                 0x2E42
#define IGPU_B43_v1                 0x2E92
#define IGPU_G41                    0x2E32
#define IGPU_G45                    0x2E22
#define IGPU_G43                    0x2E22
#define IGPU_Q45                    0x2E12
#define IGPU_Q43                    0x2E12
#define IGPU_IDEVICE                0x2E02
#define IGPU_GM45_EXPRESS           0x2A42

/**
 * iGPU series: Intel Ironlake
*/
#define IGPU_IRONLAKE_HD_v0         0x0042
#define IGPU_IRONLAKE_HD_v1         0x0046

/**
 * iGPU series: Intel Sandybridge 
*/
#define IGPU_SANDY_HD_2000_v0       0x0102
#define IGPU_SANDY_HD_2000_v1       0x0106
#define IGPU_SANDY_HD_2000_v2       0x010A

#define IGPU_SANDY_HD_3000_v0       0x0112
#define IGPU_SANDY_HD_3000_v1       0x0122
#define IGPU_SANDY_HD_3000_v2       0x0116
#define IGPU_SANDY_HD_3000_v3       0x0126

/**
 * iGPU series: Intel Ivybridge
*/
#define IGPU_IVY_HD                 0x015A

#define IGPU_IVY_HD_2500_v0         0x0152
#define IGPU_IVY_HD_2500_v1         0x0156

#define IGPU_IVY_HD_P4000           0x016A

#define IGPU_IVY_HD_4000_v0         0x0162
#define IGPU_IVY_HD_4000_v1         0x0166

/**
 * iGPU serious: Intel Haswell
*/
#define IGPU_HASWELL_HD_v0          0x0402
#define IGPU_HASWELL_HD_v1          0x0406
#define IGPU_HASWELL_HD_v2          0x040A
#define IGPU_HASWELL_HD_v3          0x040B
#define IGPU_HASWELL_HD_v4          0x040E
#define IGPU_HASWELL_HD_v5          0x0C02
#define IGPU_HASWELL_HD_v6          0x0C06
#define IGPU_HASWELL_HD_v7          0x0C0A
#define IGPU_HASWELL_HD_v8          0x0C0B
#define IGPU_HASWELL_HD_v9          0x0C0E
#define IGPU_HASWELL_HD_v10         0x0A02
#define IGPU_HASWELL_HD_v11         0x0A06
#define IGPU_HASWELL_HD_v12         0x0A0A
#define IGPU_HASWELL_HD_v13         0x0A0B
#define IGPU_HASWELL_HD_v14         0x0A0E
#define IGPU_HASWELL_HD_v15         0x0D02
#define IGPU_HASWELL_HD_v16         0x0D06
#define IGPU_HASWELL_HD_v17         0x0D0A
#define IGPU_HASWELL_HD_v18         0x0D0B
#define IGPU_HASWELL_HD_v19         0x0D0E

#define IGPU_HASWELL_HD_v20         0x041B
#define IGPU_HASWELL_HD_v21         0x0C12
#define IGPU_HASWELL_HD_v22         0x0C16
#define IGPU_HASWELL_HD_v23         0x0C1A
#define IGPU_HASWELL_HD_v24         0x0C1B
#define IGPU_HASWELL_HD_v25         0x0C1E
#define IGPU_HASWELL_HD_v26         0x0A12
#define IGPU_HASWELL_HD_v27         0x0A1A
#define IGPU_HASWELL_HD_v28         0x0A1B
#define IGPU_HASWELL_HD_v29         0x0D16
#define IGPU_HASWELL_HD_v30         0x0D1A
#define IGPU_HASWELL_HD_v31         0x0D1B
#define IGPU_HASWELL_HD_v32         0x0D1E

#define IGPU_HASWELL_HD_v33         0x0422
#define IGPU_HASWELL_HD_v34         0x0426
#define IGPU_HASWELL_HD_v35         0x042A
#define IGPU_HASWELL_HD_v36         0x042B
#define IGPU_HASWELL_HD_v37         0x042E
#define IGPU_HASWELL_HD_v38         0x0C22
#define IGPU_HASWELL_HD_v39         0x0C26
#define IGPU_HASWELL_HD_v40         0x0C2B
#define IGPU_HASWELL_HD_v41         0x0C2E
#define IGPU_HASWELL_HD_v42         0x0A22
#define IGPU_HASWELL_HD_v43         0x0A2A
#define IGPU_HASWELL_HD_v44         0x0A2B
#define IGPU_HASWELL_HD_v45         0x0D2A
#define IGPU_HASWELL_HD_v46         0x0D2B
#define IGPU_HASWELL_HD_v47         0x0D2E

#define IGPU_HASWELL_HD_4200        0x0A1E

#define IGPU_HASWELL_HD_4400_v0     0x041E
#define IGPU_HASWELL_HD_4400_v1     0x0A16

#define IGPU_HASWELL_HD_P4600       0x041A
#define IGPU_HASWELL_HD_P4700       0x041A

#define IGPU_HASWELL_HD_4600_v0     0x0412
#define IGPU_HASWELL_HD_4600_v1     0x0416
#define IGPU_HASWELL_HD_4600_v2     0x0D12

#define IGPU_HASWELL_IRISP_P5200    0x0D26
#define IGPU_HASWELL_IRISP_5200     0x0D22
#define IGPU_HASWELL_IRIS_5100      0x0A2E
#define IGPU_HASWELL_HD_5000        0x0A26

/**
 * iGPU series: Intel Bay Trail
*/
#define IGPU_BAY_HD_v0              0x0F31
#define IGPU_BAY_HD_v1              0x0F32
#define IGPU_BAY_HD_v2              0x0F33
#define IGPU_BAY_HD_v3              0x0157
#define IGPU_BAY_HD_v4              0x0155

/**
 * iGPU series: Intel Cherryview 
*/
/** OOoooooOO Intel */
#define IGPU_CHERRY_HD_XXX          0x22B1

#define IGPU_CHERRY_HD_v0           0x22B0
#define IGPU_CHERRY_HD_v1           0x22B2
#define IGPU_CHERRY_HD_v2           0x22B3

/**
 * iGPU series: Intel Boardwell
*/
#define IGPU_BOARDWELL_HD_v0        0x1602
#define IGPU_BOARDWELL_HD_v1        0x1606
#define IGPU_BOARDWELL_HD_v2        0x160A
#define IGPU_BOARDWELL_HD_v3        0x160B
#define IGPU_BOARDWELL_HD_v4        0x160D
#define IGPU_BOARDWELL_HD_v5        0x160E
#define IGPU_BOARDWELL_HD_v6        0x161B
#define IGPU_BOARDWELL_HD_v7        0x161D
#define IGPU_BOARDWELL_HD_v8        0x162D
#define IGPU_BOARDWELL_HD_v9        0x162E

#define IGPU_BOARDWELL_HD_5300      0x161E
#define IGPU_BOARDWELL_HD_P5700     0x161A
#define IGPU_BOARDWELL_HD_5500      0x1616
#define IGPU_BOARDWELL_HD_5600      0x1612
#define IGPU_BOARDWELL_HD_6000      0x1626
#define IGPU_BOARDWELL_IRIS_6100    0x162B
#define IGPU_BOARDWELL_IRISP_P6300  0x162A
#define IGPU_BOARDWELL_IRISP_6200   0x1622

/**
 * iGPU series: Intel Sky Lake
*/
#define IGPU_SKYLAKE_HD_v0          0x190A
#define IGPU_SKYLAKE_HD_v1          0x190E
#define IGPU_SKYLAKE_HD_v3          0x1913
#define IGPU_SKYLAKE_HD_v4          0x1915
#define IGPU_SKYLAKE_HD_v5          0x1917
#define IGPU_SKYLAKE_HD_v6          0x191A
#define IGPU_SKYLAKE_HD_v7          0x192A

#define IGPU_SKYLAKE_HD_510_v0      0x1902
#define IGPU_SKYLAKE_HD_510_v1      0x1906
#define IGPU_SKYLAKE_HD_510_v2      0x190B

#define IGPU_SKYLAKE_HD_515         0x191E
#define IGPU_SKYLAKE_HD_P530        0x191D

#define IGPU_SKYLAKE_HD_520_v0      0x1916
#define IGPU_SKYLAKE_HD_520_v1      0x1921

#define IGPU_SKYLAKE_HD_530_v0      0x1912
#define IGPU_SKYLAKE_HD_530_v1      0x191B

#define IGPU_SKYLAKE_IRIS_P555      0x192D
#define IGPU_SKYLAKE_IRIS_555       0x192B
#define IGPU_SKYLAKE_IRIS_550       0x1927
#define IGPU_SKYLAKE_IRIS_540       0x1926
#define IGPU_SKYLAKE_HD_535         0x1923

#define IGPU_SKYLAKE_IRIS_P580_v0   0x193A
#define IGPU_SKYLAKE_IRIS_P580_v1   0x193D

#define IGPU_SKYLAKE_IRISP_580_v0   0x1932
#define IGPU_SKYLAKE_IRISP_580_v1   0x193B

/**
 * iGPU series: Intel Apollo Lake
*/
#define IGPU_APOLLO_HD_v0           0x0A84
#define IGPU_APOLLO_HD_v1           0x1A84
#define IGPU_APOLLO_HD_v2           0x1A85

#define IGPU_APOLLO_HD_505          0x5A84
#define IGPU_APOLLO_HD_500          0x5A85

/**
 * iGPU series: Intel Gemini Lake
*/
#define IGPU_GEMINI_UHD_605         0x3184
#define IGPU_GEMINI_UHD_600         0x3185

/**
 * iGPU series: Intel Kaby Lake
*/
#define IGPU_KABY_HD_v0             0x590A
#define IGPU_KABY_HD_v1             0x5908
#define IGPU_KABY_HD_v2             0x590E
#define IGPU_KABY_HD_v3             0x5913
#define IGPU_KABY_HD_v4             0x5915
#define IGPU_KABY_HD_v5             0x593B

#define IGPU_KABY_HD_610_v0         0x5902
#define IGPU_KABY_HD_610_v1         0x5906
#define IGPU_KABY_HD_610_v2         0x590B

#define IGPU_KABY_UHD_617           0x87C0
#define IGPU_KABY_UHD_615           0x591C
#define IGPU_KABY_HD_615            0x591E

#define IGPU_KABY_HD_P630_v0        0x591A
#define IGPU_KABY_HD_P630_v1        0x591D

#define IGPU_KABY_HD_620_v0         0x5916
#define IGPU_KABY_HD_620_v1         0x5921

#define IGPU_KABY_HD_630_v0         0x5912
#define IGPU_KABY_HD_630_v1         0x591B

#define IGPU_KABY_UHD_620           0x5917
#define IGPU_KABY_IRISP_650         0x5927
#define IGPU_KABY_IRISP_640         0x5926
#define IGPU_KABY_HD_635            0x5923

/**
 * iGPU series: Intel Coffee Lake
*/
#define IGPU_COFFEE_UHD_620_v0      0x3EA9
#define IGPU_COFFEE_UHD_620_v1      0x3EA0
#define IGPU_COFFEE_UHD_P630_v0     0x3E96
#define IGPU_COFFEE_UHD_P630_v1     0x3E9A
#define IGPU_COFFEE_UHD_P630_v2     0x9BC6
#define IGPU_COFFEE_UHD_P630_v3     0x9BE6
#define IGPU_COFFEE_UHD_P630_v4     0x9BF6
#define IGPU_COFFEE_UHD_630_v0      0x3E91
#define IGPU_COFFEE_UHD_630_v1      0x3E92
#define IGPU_COFFEE_UHD_630_v2      0x3E98
#define IGPU_COFFEE_UHD_630_v3      0x3E9B
#define IGPU_COFFEE_UHD_630_v4      0x9BC5
#define IGPU_COFFEE_UHD_630_v5      0x9BC8

#define IGPU_COFFEE_UHD_v0          0x87CA
#define IGPU_COFFEE_UHD_v1          0x3EA3
#define IGPU_COFFEE_UHD_v2          0x9B41
#define IGPU_COFFEE_UHD_v3          0x9BC0
#define IGPU_COFFEE_UHD_v4          0x9BC2
#define IGPU_COFFEE_UHD_v5          0x9BC4
#define IGPU_COFFEE_UHD_v6          0x9BCA
#define IGPU_COFFEE_UHD_v7          0x9BCB
#define IGPU_COFFEE_UHD_v8          0x9BCC
#define IGPU_COFFEE_UHD_v9          0x3EA4
#define IGPU_COFFEE_UHD_v10         0x9B21
#define IGPU_COFFEE_UHD_v11         0x9BA0
#define IGPU_COFFEE_UHD_v12         0x9BA2
#define IGPU_COFFEE_UHD_v13         0x9BA4
#define IGPU_COFFEE_UHD_v14         0x9BAA
#define IGPU_COFFEE_UHD_v15         0x9BAB
#define IGPU_COFFEE_UHD_v16         0x9BAC
#define IGPU_COFFEE_UHD_v17         0x3EA2

#define IGPU_COFFEE_UHD_610_v0      0x3E90
#define IGPU_COFFEE_UHD_610_v1      0x3E93
#define IGPU_COFFEE_UHD_610_v2      0x3E99
#define IGPU_COFFEE_UHD_610_v3      0x3E9C
#define IGPU_COFFEE_UHD_610_v4      0x3EA1
#define IGPU_COFFEE_UHD_610_v5      0x9BA5
#define IGPU_COFFEE_UHD_610_v6      0x9BA8

#define IGPU_COFFEE_HD              0x3EA7
#define IGPU_COFFEE_IRISP_645       0x3EA6

#define IGPU_COFFEE_IRISP_655_v0    0x3EA5
#define IGPU_COFFEE_IRISP_655_v1    0x3EA8

/**
 * iGPU series: Intel Ice Lake
*/
#define IGPU_ICE_IRISP_v0           0x8A51
#define IGPU_ICE_IRISP_v1           0x8A52
#define IGPU_ICE_IRISP_v2           0x8A53
#define IGPU_ICE_IRISP_v3           0x8A54
#define IGPU_ICE_IRISP_v4           0x8A5A
#define IGPU_ICE_IRISP_v5           0x8A5C

#define IGPU_ICE_HD_v0              0x8A50
#define IGPU_ICE_HD_v1              0x8A57
#define IGPU_ICE_HD_v2              0x8A59
#define IGPU_ICE_HD_v3              0x8A5B
#define IGPU_ICE_HD_v4              0x8A5D
#define IGPU_ICE_HD_v5              0x8A71

#define IGPU_ICE_UHD_v0             0x8A56
#define IGPU_ICE_UHD_v1             0x8A58

/**
 * iGPU series: Intel Tiger Lake
*/
#define IGPU_TIGER_UHD_v0           0x9A78
#define IGPU_TIGER_UHD_v1           0x9AC0
#define IGPU_TIGER_UHD_v2           0x9AC9
#define IGPU_TIGER_UHD_v3           0x9AD9
#define IGPU_TIGER_UHD_v4           0x9AF8
#define IGPU_TIGER_UHD_v5           0x9A60
#define IGPU_TIGER_UHD_v6           0x9A68
#define IGPU_TIGER_UHD_v7           0x9A70

#define IGPU_TIGER_IRISX_v0         0x9A40
#define IGPU_TIGER_IRISX_v1         0x9A49

#endif
