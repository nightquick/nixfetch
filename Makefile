CC ?= gcc
PROG = nixfetch
# You need to install development branch of pciutils
FLAGS = -O2 -Wall -s -lpci
VERSION = $(shell git rev-parse --short HEAD)

all:
	@$(CC) $(PROG).c  -DVERSION=\"1.3-$(VERSION)\" $(FLAGS) -o $(PROG)

clean:
	@rm nixfetch
