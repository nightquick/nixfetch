#ifndef NIXFETCH_H
#define NIXFETCH_H

#include <stdio.h>
#include <stdarg.h>

#include "cpuid.h"
#include "intel_igpu.h"

#ifndef PCI_VGA_CONTROLLER
    #define PCI_VGA_CONTROLLER  0x0300
#endif

#ifndef PCI_VENDOR_ID_AMD
    #define PCI_VENDOR_ID_AMD   0x1022
#endif

#ifndef PCI_VENDOR_ID_CYRIX
    #define PCI_VENDOR_ID_CYRIX 0x1078
#endif

#ifndef BUFFS
    #define BUFFS    200
#endif

/** Undefine anything that can conflict with current constants */
#undef WHITE
#undef RED
#undef GREEN
#undef OFF
#define WHITE   "\033[0;97m"
#define RED     "\033[0;91m"
#define GREEN   "\033[0;92m"
#define OFF     "\033[0m"

#ifndef ARRAY_SIZE
    #define ARRAY_SIZE(arr) sizeof(arr) / sizeof(arr[0])
#endif

/** iGPU codename and the GPU name itself */
struct LupEntry {
    int value;
    char cname[25];
    char igpu[50];
};

/** Main NixFetch structure */
struct NixFet {
    char brand_str[BUFFS];
    char label_str[BUFFS];
    unsigned int stepping_id;
    char cpu_type[BUFFS];
    unsigned int extended_model_id;
    unsigned int extended_family_id;
    unsigned int cpu_cores;
    char cpu_extensions[BUFFS];
    char igpu_vendor[BUFFS];
    char igpu_id[BUFFS];
    char bios_date[BUFFS];
    char bios_version[BUFFS];
    char bios_vendor[BUFFS];
    char sys_loads[BUFFS];
    char sys_uptime[BUFFS];
    char distro_name[BUFFS];
    char ram_info[BUFFS];
    char shell_name[BUFFS];
    char user_name[BUFFS];
    int user_uid;
    char host_name[BUFFS];
    char kern_arch[BUFFS];
    char kern_release[BUFFS];
};

/**
 * List of Intel iGPU
*/
struct LupEntry lup_table[] = {
    { IGPU_SANDY_HD_2000_v0, "Sandy Bridge", "Intel HD Graphics 2000" },
    { IGPU_SANDY_HD_2000_v1, "Sandy Bridge", "Intel HD Graphics 2000" },
    { IGPU_SANDY_HD_2000_v2, "Sandy Bridge", "Intel HD Graphics 2000" },
    { IGPU_SANDY_HD_3000_v0, "Sandy Bridge", "Intel HD Graphics 3000" },
    { IGPU_SANDY_HD_3000_v1, "Sandy Bridge", "Intel HD Graphics 3000" },
    { IGPU_SANDY_HD_3000_v2, "Sandy Bridge", "Intel HD Graphics 3000" },

    { IGPU_IVY_HD, "Ivy Bridge", "Intel HD Graphics Family" },
    { IGPU_IVY_HD_2500_v0, "Ivy Bridge", "Intel HD Graphics 2500" },
    { IGPU_IVY_HD_2500_v1, "Ivy Bridge", "Intel HD Graphics 2500" },
    { IGPU_IVY_HD_P4000, "Ivy Bridge", "Intel HD Graphics P4000" },
    { IGPU_IVY_HD_P4000, "Ivy Bridge", "Intel HD Graphics P4000" },
    { IGPU_IVY_HD_4000_v0, "Ivy Bridge", "Intel HD Graphics 4000" },
    { IGPU_IVY_HD_4000_v1, "Ivy Bridge", "Intel HD Graphics 4000" },

    { IGPU_HASWELL_HD_v0, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v1, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v2, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v20, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v21, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v33, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_v34, "Haswell", "Intel HD Graphics" },
    { IGPU_HASWELL_HD_4200, "Haswell", "Intel HD Graphics 4200" },
    { IGPU_HASWELL_HD_4400_v0, "Haswell", "Intel HD Graphics 4400" },
    { IGPU_HASWELL_HD_P4600, "Haswell", "Intel HD Graphics P4600/P4700" },
    { IGPU_HASWELL_HD_P4700, "Haswell", "Intel HD Graphics P4600/P4700" },
    { IGPU_HASWELL_HD_4600_v0, "Haswell", "Intel HD Graphics 4600" },
    { IGPU_HASWELL_HD_4600_v1, "Haswell", "Intel HD Graphics 4600" },
    { IGPU_HASWELL_HD_4600_v0, "Haswell", "Intel HD Graphics 4600" },
    { IGPU_HASWELL_IRISP_P5200, "Haswell", "Intel Iris Pro Graphics P5200" },
    { IGPU_HASWELL_IRISP_5200, "Haswell", "Intel Iris Pro Graphics 5200" },
    { IGPU_HASWELL_IRIS_5100, "Haswell", "Intel Iris Graphics 5100" },
    { IGPU_HASWELL_HD_5000, "Haswell", "Intel HD Graphics 5000" },

    { IGPU_BAY_HD_v0, "Bay Trail", "Intel HD Graphics" },
    { IGPU_BAY_HD_v1, "Bay Trail", "Intel HD Graphics" },

    { IGPU_CHERRY_HD_XXX, "Cherryview", "Intel HD Graphics XXX" },
    { IGPU_CHERRY_HD_v0, "Cherryview", "Intel HD Graphics" },
    { IGPU_CHERRY_HD_v1, "Cherryview", "Intel HD Graphics" },

    { IGPU_BOARDWELL_HD_v0, "Broadwell", "Intel HD Graphics" },
    { IGPU_BOARDWELL_HD_v1, "Broadwell", "Intel HD Graphics" },
    { IGPU_BOARDWELL_HD_5300, "Broadwell", "Intel HD Graphics 5300" },
    { IGPU_BOARDWELL_HD_P5700, "Broadwell", "Intel HD Graphics P5700" },
    { IGPU_BOARDWELL_HD_5500, "Broadwell", "Intel HD Graphics 5500" },
    { IGPU_BOARDWELL_HD_5600, "Broadwell", "Intel HD Graphics 5600" },
    { IGPU_BOARDWELL_HD_6000, "Broadwell", "Intel HD Graphics 6000" },
    { IGPU_BOARDWELL_IRIS_6100, "Broadwell", "Intel Iris Graphics 6100" },
    { IGPU_BOARDWELL_IRISP_P6300, "Broadwell", "Intel Iris Pro Graphics P6300" },
    { IGPU_BOARDWELL_IRISP_6200, "Broadwell", "Intel Iris Pro Graphics 6200" },

    { IGPU_SKYLAKE_HD_v0, "Skylake", "Intel HD Graphics" },
    { IGPU_SKYLAKE_HD_510_v0, "Skylake", "Intel HD Graphics 510" },
    { IGPU_SKYLAKE_HD_510_v1, "Skylake", "Intel HD Graphics 510" },
    { IGPU_SKYLAKE_HD_515, "Skylake", "Intel HD Graphics 515" },
    { IGPU_SKYLAKE_HD_P530, "Skylake", "Intel HD Graphics P530" },
    { IGPU_SKYLAKE_HD_520_v0, "Skylake", "Intel HD Graphics 520" },
    { IGPU_SKYLAKE_HD_520_v1, "Skylake", "Intel HD Graphics 520" },
    { IGPU_SKYLAKE_HD_530_v0, "Skylake", "Intel HD Graphics 530" },
    { IGPU_SKYLAKE_HD_530_v1, "Skylake", "Intel HD Graphics 530" },
    { IGPU_SKYLAKE_IRIS_P555, "Skylake", "Intel Iris Graphics P555" },
    { IGPU_SKYLAKE_IRIS_555, "Skylake", "Intel Iris Graphics 555" },
    { IGPU_SKYLAKE_IRIS_550, "Skylake", "Intel Iris Graphics 550" },
    { IGPU_SKYLAKE_IRIS_540, "Skylake", "Intel Iris Graphics 540" },
    { IGPU_SKYLAKE_HD_535, "Skylake", "Intel HD Graphics 535" },
    { IGPU_SKYLAKE_IRIS_P580_v0, "Skylake", "Intel Iris Pro Graphics P580" },
    { IGPU_SKYLAKE_IRIS_P580_v1, "Skylake", "Intel Iris Pro Graphics P580" },
    { IGPU_SKYLAKE_IRISP_580_v0, "Skylake", "Intel Iris Pro Graphics 580" },
    { IGPU_SKYLAKE_IRISP_580_v1, "Skylake", "Intel Iris Pro Graphics 580" },

    { IGPU_APOLLO_HD_v0, "Apollo Lake", "Intel HD Graphics" },
    { IGPU_APOLLO_HD_v2, "Apollo Lake", "Intel HD Graphics" },
    { IGPU_APOLLO_HD_505, "Apollo Lake", "Intel HD Graphics 505" },
    { IGPU_APOLLO_HD_500, "Apollo Lake", "Intel HD Graphics 500" },

    { IGPU_GEMINI_UHD_605, "Gemini Lake", "Intel UHD Graphics 605" },
    { IGPU_GEMINI_UHD_600, "Gemini Lake", "Intel UHD Graphics 600" },

    { IGPU_KABY_HD_v0, "Kaby Lake", "Intel HD Graphics" },
    { IGPU_KABY_HD_v1, "Kaby Lake", "Intel HD Graphics" },
    { IGPU_KABY_HD_610_v0, "Kaby Lake", "Intel HD Graphics 610" },
    { IGPU_KABY_HD_610_v1, "Kaby Lake", "Intel HD Graphics 610" },
    { IGPU_KABY_UHD_617, "Kaby Lake", "Intel UHD Graphics 617" },
    { IGPU_KABY_UHD_615, "Kaby Lake", "Intel UHD Graphics 615" },
    { IGPU_KABY_HD_615, "Kaby Lake", "Intel HD Graphics 615" },
    { IGPU_KABY_HD_P630_v0, "Kaby Lake", "Intel HD Graphics P630" },
    { IGPU_KABY_HD_P630_v1, "Kaby Lake", "Intel HD Graphics P630" },
    { IGPU_KABY_HD_620_v0, "Kaby Lake", "Intel HD Graphics 620" },
    { IGPU_KABY_HD_620_v1, "Kaby Lake", "Intel HD Graphics 620" },
    { IGPU_KABY_HD_630_v0, "Kaby Lake", "Intel HD Graphics 630" },
    { IGPU_KABY_HD_630_v1, "Kaby Lake", "Intel HD Graphics 630" },
    { IGPU_KABY_UHD_620, "Kaby Lake", "Intel UHD Graphics 620" },
    { IGPU_KABY_IRISP_650, "Kaby Lake", "Intel Iris Plus Graphics 650" },
    { IGPU_KABY_IRISP_640, "Kaby Lake", "Intel Iris Plus Graphics 640" },
    { IGPU_KABY_HD_635, "Kaby Lake", "Intel HD Graphics 635" },

    { IGPU_COFFEE_UHD_620_v0, "Coffee Lake", "Intel UHD Graphics 620" },
    { IGPU_COFFEE_UHD_620_v1, "Coffee Lake", "Intel UHD Graphics 620" },
    { IGPU_COFFEE_UHD_P630_v0, "Coffee Lake", "Intel UHD Graphics P630" },
    { IGPU_COFFEE_UHD_P630_v1, "Coffee Lake", "Intel UHD Graphics P630" },
    { IGPU_COFFEE_UHD_630_v0, "Coffee Lake", "Intel UHD Graphics 630" },
    { IGPU_COFFEE_UHD_630_v1, "Coffee Lake", "Intel UHD Graphics 630" },
    { IGPU_COFFEE_UHD_v0, "Coffee Lake", "Intel UHD Graphics" },
    { IGPU_COFFEE_UHD_v1, "Coffee Lake", "Intel UHD Graphics" },
    { IGPU_COFFEE_UHD_v2, "Coffee Lake", "Intel UHD Graphics" },
    { IGPU_COFFEE_UHD_610_v0, "Coffee Lake", "Intel UHD Graphics 610" },
    { IGPU_COFFEE_UHD_610_v1, "Coffee Lake", "Intel UHD Graphics 610" },
    { IGPU_COFFEE_HD, "Coffee Lake", "Intel HD Graphics" },
    { IGPU_COFFEE_IRISP_645, "Coffee Lake", "Intel Iris Plus Graphics 645" },
    { IGPU_COFFEE_IRISP_655_v0, "Coffee Lake", "Intel Iris Plus Graphics 655" },
    { IGPU_COFFEE_IRISP_655_v1, "Coffee Lake", "Intel Iris Plus Graphics 655" },

    { IGPU_ICE_IRISP_v0, "Ice Lake", "Intel Iris Plus Graphics" },
    { IGPU_ICE_IRISP_v1, "Ice Lake", "Intel Iris Plus Graphics" },
    { IGPU_ICE_IRISP_v2, "Ice Lake", "Intel Iris Plus Graphics" },
    { IGPU_ICE_IRISP_v3, "Ice Lake", "Intel Iris Plus Graphics" },
    { IGPU_ICE_IRISP_v4, "Ice Lake", "Intel Iris Plus Graphics" },
    { IGPU_ICE_IRISP_v5, "Ice Lake", "Intel Iris Plus Graphics" },

    { IGPU_ICE_HD_v0, "Ice Lake", "Intel HD Graphics" },
    { IGPU_ICE_HD_v1, "Ice Lake", "Intel HD Graphics" },
    { IGPU_ICE_HD_v2, "Ice Lake", "Intel HD Graphics" },
    { IGPU_ICE_HD_v3, "Ice Lake", "Intel HD Graphics" },    
    { IGPU_ICE_UHD_v0, "Ice Lake", "Intel UHD Graphics" },
    { IGPU_ICE_UHD_v1, "Ice Lake", "Intel UHD Graphics" },

    { IGPU_TIGER_UHD_v0, "Tiger Lake", "Intel UHD Graphics" },
    { IGPU_TIGER_UHD_v1, "Tiger Lake", "Intel UHD Graphics" },
    { IGPU_TIGER_UHD_v2, "Tiger Lake", "Intel UHD Graphics" },
    { IGPU_TIGER_UHD_v3, "Tiger Lake", "Intel UHD Graphics" },
    { IGPU_TIGER_UHD_v4, "Tiger Lake", "Intel UHD Graphics" },
    { IGPU_TIGER_IRISX_v0, "Tiger Lake", "Intel Iris Xe Graphics" },
    { IGPU_TIGER_IRISX_v1, "Tiger Lake", "Intel Iris Xe Graphics" }
};

static int pick_color = 0;

/**
 * Wrap perror and exit one function
*/
static void error(int status, char *msg)
{
    perror(msg);
    exit(status);
}

/**
 * Wrap vfprintf() for customization
*/
static void print(char *fmt, ...)
{
    va_list	listp;

	va_start(listp, fmt);

    if (pick_color > 0) {
        char *ch_color = "\033[0;93m";

        switch (pick_color) {
            case 1: ch_color = "\033[0;91m"; break;
            case 2: ch_color = "\033[0;92m"; break;
            case 3: ch_color = "\033[0;93m"; break;
            case 4: ch_color = "\033[0;94m"; break;
            case 5: ch_color = "\033[0;95m"; break;
            case 6: ch_color = "\033[0;96m"; break;
            case 7: default: ch_color = "\033[0;97m"; break;
        }
        vfprintf(stdout, ch_color, listp);
        vfprintf(stdout, fmt, listp);
        vfprintf(stdout, "\033[0m", listp);
    } else {
        vfprintf(stdout, fmt, listp);
    }
    va_end(listp);
}

/**
 * Trim all spaces (start and end)
*/
static char *trim_spaces(char *str)
{
    char *src = str, *dst = str;

    while (*src != '\0' && *src == ' ')
        src++;

    while (*src != '\0' && *src == '\n')
        src++;

    while (*src != '\0') {
        while (*src == ' ' && *(src + 1) == ' ')
            src++;
        *dst++ = *src++;
    }

    *dst = '\0';

    return str;
}

/**
 * Print text according iterations
*/
static char *print_base(int len, const char *msg)
{
    char *tb = malloc(BUFFS);

    if (tb == NULL)
        error(EXIT_FAILURE, "malloc():");

    memset(tb, '\0', sizeof(&tb));
    for (int i = 0; i < len + 1; i++)
        strcat(tb, msg);

    return tb;
}

/**
 * Read a line from the file
*/
static char *read_oneline(char *file)
{
    FILE *fp;
    char *buf = malloc(BUFFS);

    if (buf == NULL)
        error(EXIT_FAILURE, "malloc():");

    memset(buf, '\0', BUFFS);
    fp = fopen(file, "rb");

    if (fp == NULL)
        error(
            EXIT_FAILURE, "No DMI information found. Are you sure, you're running Linux kernel 2.6.23 or above?"
        );

    fread(buf, 1, BUFFS, fp);
    fclose(fp);

    for (int i = 0; i < BUFFS; i++)
        if (buf[i] == '\n')
            buf[i] = '\0';

    return buf;
}

/**
 * Find iGPU information
*/
static char *igpu_find(int value)
{
    long unsigned int i;
    char *buf = malloc(BUFFS);

    if (buf == NULL)
        error(EXIT_FAILURE, "malloc():");

    memset(buf, '\0', BUFFS);

    for (i = 0; i < ARRAY_SIZE(lup_table); i++) {
        if (lup_table[i].value == value) {
            strcat(buf, lup_table[i].igpu);
            strcat(buf, " (");
            strcat(buf, lup_table[i].cname);
            strcat(buf, ")");
            return buf;
        }
    }

    return "NOT_IMPLEMENTED";
}

#endif
