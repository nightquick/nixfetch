/**
 * Author: rilysh <nightquick@proton.me>
 * License: BSD-2-Clause
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <glob.h>
#include <pwd.h>
#include <getopt.h>
#include <sys/sysinfo.h>
#include <sys/utsname.h>
#include <pci/pci.h>

#include "nixfetch.h"
#include "intel_igpu.h"

/* A global alike array to hold the buffer for each function where is implemented */
static struct NixFet nix;

/**
 * On Intel platform it would be "GenuineIntel"
 * On AMD platform it would be "AuthenticAMD", K5 or earlier CPUs will have "AMDisbetter!"
*/
static void get_cpu_brand(void)
{
    unsigned int reg[4];

    __cpuid(0, reg[0], reg[1], reg[2], reg[3]);

    memcpy(nix.brand_str, &reg[1], sizeof(reg[1]));
    memcpy(nix.brand_str + 4, &reg[3], sizeof(reg[3]));
    memcpy(nix.brand_str + 8, &reg[2], sizeof(reg[2]));

    nix.brand_str[12] = '\0';
}

/**
 * Get the CPU name, for e.g. Intel(R) Core(TM) i3-3240 CPU @ 3.40GHz
*/
static void get_cpu_label(void)
{
    unsigned int reg[4];
    int i, p = 0;

    __cpuid(0x80000000, reg[0], reg[1], reg[2], reg[3]);

    if (!(reg[0] & 0x80000000))
        error(EXIT_FAILURE, "__cpuid() extended:");

    for (i = 0; i < 3; i++) {
        reg[0] = 0x80000002 + i;
        __cpuid(reg[0], reg[0], reg[1], reg[2], reg[3]);

        nix.label_str[p++] = reg[0] & 0xff;
        nix.label_str[p++] = (reg[0] >> 8) & 0xff;
        nix.label_str[p++] = (reg[0] >> 16) & 0xff;
        nix.label_str[p++] = (reg[0] >> 24) & 0xff;

        nix.label_str[p++] = reg[1] & 0xff;
        nix.label_str[p++] = (reg[1] >> 8) & 0xff;
        nix.label_str[p++] = (reg[1] >> 16) & 0xff;
        nix.label_str[p++] = (reg[1] >> 24) & 0xff;

        nix.label_str[p++] = reg[2] & 0xff;
        nix.label_str[p++] = (reg[2] >> 8) & 0xff;
        nix.label_str[p++] = (reg[2] >> 16) & 0xff;
        nix.label_str[p++] = (reg[2] >> 24) & 0xff;
        nix.label_str[p++] = reg[3] & 0xff;

        nix.label_str[p++] = (reg[3] >> 8) & 0xff;
        nix.label_str[p++] = (reg[3] >> 16) & 0xff;
        nix.label_str[p++] = (reg[3] >> 24) & 0xff; 
    }

    nix.label_str[p] = '\0';
    trim_spaces(nix.label_str);
}

/**
 * Get CPU stepping ID
*/
static void get_stepping_id(void)
{
    unsigned int reg[4];
 
    __cpuid(1, reg[0], reg[1], reg[2], reg[3]);

    /**
    * After executing cpuid with eax 1 I got
    * 0x00000001 0x00: eax=0x000306a9 ebx=0x01100800 ecx=0x3d9ae3bf edx=0xbfebfbff
    * So the trick is that we need to use this first eax value, and bitand with last hex charecter
    */
    nix.stepping_id = reg[0] & 0x0f;
}

/**
 * Note: This is an early implementation for Intel CPUs. AMD doesn't yet have
 * any of these specifications regarding OEM detection.
*/
static void get_cpu_type(void)
{
    unsigned int reg[4];

    __cpuid(1, reg[0], reg[1], reg[2], reg[3]);

    unsigned int type = (reg[0] >> 12) & 0x03;

    switch (type) {
    case 00:
        sprintf(nix.cpu_type, "%s", "OEM");
        break;

    case 01:
        sprintf(nix.cpu_type, "%s", "Intel Overdrive");
        break;

    case 10:
        sprintf(nix.cpu_type, "%s", "Dual CPU");
        break;

    case 11:
        sprintf(nix.cpu_type, "%s", "Reserved");
        break;

    default:
        sprintf(nix.cpu_type, "%s", "Unidentified");
        break;
    }
}

/**
 * Get CPU extended model ID
*/
static void get_extended_model(void)
{
    unsigned int reg[4];
    __cpuid(0x80000000, reg[0], reg[1], reg[2], reg[3]);

    if (!(reg[0] & 0x80000000))
        error(EXIT_FAILURE, "__cpuid() extended:");

    __cpuid(1, reg[0], reg[1], reg[2], reg[3]);

    nix.extended_model_id = (reg[0] >> 16) & 0xff;
}

/**
 * Get CPU extended family ID
*/
static void get_extended_family(void)
{
    unsigned int reg[4];
    __cpuid(0x80000000, reg[0], reg[1], reg[2], reg[3]);

    if (!(reg[0] & 0x80000000))
        error(EXIT_FAILURE, "__cpuid() extended:");

    __cpuid(1, reg[0], reg[1], reg[2], reg[3]);

    nix.extended_family_id = (reg[0] >> 20) & 0xff;
}

/**
 * Note: This implementation might not be the one you want. It's only supported
 * in Intel CPUs. Newer Intel CPUs might have a different approach.
 * In future, I might change this implementation with sysconf()
*/
static void get_cpu_cores(void)
{
    unsigned int tmp_reg[4];
    unsigned int reg[4];

    reg[2] = 1;

     __cpuid(0, tmp_reg[0], tmp_reg[1], tmp_reg[2], tmp_reg[3]);

    /**
     * Matching the first letter 'G', also 0x756e6547 is equivalent to charecter 'G'
    */
    if (tmp_reg[1] == 0x756e6547) {
        __cpuid_count(11, reg[2], reg[0], reg[1], reg[2], reg[3]);
        nix.cpu_cores = reg[0];
    }
}

/**
 * Get CPU supported SIMD extensions
*/
static void get_cpu_extensions(void)
{
    /**
     * If bitwise and results zero, add that to cpu extensions list
    */
#define IF_BIT_ZERO(pos, bit, name) \
    if (pos & bit) \
        strcat(nix.cpu_extensions, name);

    unsigned int reg[4];
    unsigned int tmp_reg0, tmp_reg1;

    __cpuid(0, reg[0], reg[1], reg[2], reg[3]);
    tmp_reg0 = reg[0];
    
    __cpuid(0x80000000, reg[0], reg[1], reg[2], reg[3]);
    tmp_reg1 = reg[0];

    if (tmp_reg0 >= 1) {
        __cpuid(1, reg[0], reg[1], reg[2], reg[3]);

        IF_BIT_ZERO(reg[3], BIT_SSE, "SSE");
        IF_BIT_ZERO(reg[3], BIT_SSE2, " SSE2");
        IF_BIT_ZERO(reg[3], BIT_SSE3, " SSE3");
        IF_BIT_ZERO(reg[2], BIT_SSSE3, " SSSE3");
        IF_BIT_ZERO(reg[2], BIT_SSE41, " SSE4.1");
        IF_BIT_ZERO(reg[2], BIT_SSE42, " SSE4.2");

        if (tmp_reg1 >= 0x80000001) {
            __cpuid(0x80000001, reg[0], reg[1], reg[2], reg[3]);
            IF_BIT_ZERO(reg[2], BIT_SSE4a, " SSE4a");

            __cpuid(1, reg[0], reg[1], reg[2], reg[3]);
        }

        IF_BIT_ZERO(reg[3], BIT_MMX, " MMX");
        IF_BIT_ZERO(reg[2], BIT_AES, " AES");
        IF_BIT_ZERO(reg[2], BIT_AVX, " AVX");
        IF_BIT_ZERO(reg[2], BIT_FMA3, " FMA3");
        IF_BIT_ZERO(reg[2], BIT_RDRAND, " RDRAND");
    }

    if (tmp_reg0 >= 7) {
        __cpuid(7, reg[0], reg[1], reg[2], reg[3]);

        IF_BIT_ZERO(reg[1], BIT_AVX2, " AVX2");
        IF_BIT_ZERO(reg[1], BIT_ADX, " ADX");
        IF_BIT_ZERO(reg[1], BIT_SHA, " SHA");
        IF_BIT_ZERO(reg[1], BIT_AVX512CD, " AVX512CD");
        IF_BIT_ZERO(reg[1], BIT_AVX512ER, " AVX512ER");
        IF_BIT_ZERO(reg[1], BIT_AVX512F, " AVX512F");
        IF_BIT_ZERO(reg[1], BIT_AVX512PF, " AVX512PF");
        IF_BIT_ZERO(reg[1], BIT_AVX5124FMAPS, " AVX5124FMAPS");
        IF_BIT_ZERO(reg[1], BIT_AVX5124VNNIW, " AVX5124VNNIW");
        IF_BIT_ZERO(reg[1], BIT_AVX512BW, " AVX512BW");
        IF_BIT_ZERO(reg[1], BIT_AVX512DQ, " AVX512DQ");
        IF_BIT_ZERO(reg[1], BIT_AVX512IFMA, " AVX512IFMA");
        IF_BIT_ZERO(reg[1], BIT_AVX512VBMI2, " AVX512VBMI2");
        IF_BIT_ZERO(reg[1], BIT_AVX512VBMI, " AVX512VBMI");
        IF_BIT_ZERO(reg[1], BIT_AVX512VL, " AVX512VL");
        IF_BIT_ZERO(reg[1], BIT_AVX512VNNI, " AVX512VNNI");
        IF_BIT_ZERO(reg[1], BIT_AVX512VPOPCNTDQ, " AVX512VPOPCNTDQ");
    }

    trim_spaces(nix.cpu_extensions);
}

/**
 * Get iGPU information
 * Note: Only Intel iGPUs are currently supported. According to AMD,
 * only a handful of AMD CPU does have iGPU. But at this moment,
 * AMD Ryzen CPU doesn't seems like have one or I'm quite not sure about it. 
*/
static void get_igpu_info(void)
{
    struct pci_access *pa;
    struct pci_dev *pd;
    char *igpu_f;
    int type = PCI_FILL_BASES | PCI_FILL_CLASS | PCI_FILL_IDENT | PCI_FILL_CLASS;

    pa = pci_alloc();
    pci_init(pa);
    pci_scan_bus(pa);
    pci_fill_info(pa->devices, type);

    pd = pa->devices;
    igpu_f = igpu_find(pd->device_id);

    if (pd->device_class == PCI_VGA_CONTROLLER) {
        if (pd->vendor_id == PCI_VENDOR_ID_INTEL) {
            sprintf(nix.igpu_vendor, "%s", "Intel");
            sprintf(nix.igpu_id, "%s", igpu_f);
        } else if (pd->vendor_id == PCI_VENDOR_ID_AMD) {
            sprintf(nix.igpu_vendor, "%s", "AMD");
            sprintf(nix.igpu_id, "%s", "Unknown");
        } else if (pd->vendor_id == PCI_VENDOR_ID_CYRIX) {
            sprintf(nix.igpu_vendor, "%s", "Cyrix");
            sprintf(nix.igpu_id, "%s", "Unknown");
        } else {
            sprintf(nix.igpu_vendor, "%s", "Unidentified");
            sprintf(nix.igpu_id, "%s", "Unidentified");   
        }
    } else {
        sprintf(nix.igpu_vendor, "%s", "Unidentified");
        sprintf(nix.igpu_id, "%s", "Unidentified");    
    }

    pci_cleanup(pa);
    free(igpu_f);
}

/**
 * Get information about your PC BIOS
*/
static void get_bios_info(void)
{
    char *bios_date = read_oneline(
        "/sys/devices/virtual/dmi/id/bios_date"
    );
    char *bios_vendor = read_oneline(
        "/sys/devices/virtual/dmi/id/bios_vendor"
    );
    char *bios_version = read_oneline(
        "/sys/devices/virtual/dmi/id/bios_version"
    );

    sprintf(
        nix.bios_date, "%s",
        strlen(bios_date) == 0 ? "Unspecified" : bios_date
    );
    sprintf(
        nix.bios_vendor, "%s",
        strlen(bios_vendor) == 0 ? "Generic BIOS" : bios_vendor
    );
    sprintf(
        nix.bios_version, "%s",
        strlen(bios_version) == 0 ? "Unspecified" : bios_version
    );

    free(bios_date);
    free(bios_vendor);
    free(bios_version);
}

/**
 * Get system load information
*/
static void get_sys_loads(void)
{
    struct sysinfo si;

    if (sysinfo(&si) != 0)
        error(EXIT_FAILURE, "sysinfo():");

    float sl = 1.0 * (1 << SI_LOAD_SHIFT);

    sprintf(nix.sys_loads, "%.2lf %.2lf %.2lf",
            si.loads[0] / sl,
            si.loads[1] / sl,
            si.loads[2] / sl
    );
}

/**
 * Get system uptime
*/
static void get_sys_uptime(int expanded)
{
    unsigned int days, hours, mins, secs;
    struct sysinfo si;

    if (sysinfo(&si) != 0)
        error(EXIT_FAILURE, "sysinfo():");

    days = si.uptime / (60 * 60 * 24);
    hours = (si.uptime / (60 * 60)) - (days * 24);
    mins = (si.uptime / 60) - (days * 60 * 24) - (hours * 60);
    secs = si.uptime - (days * 60 * 60 * 24) - (hours * 60 * 60) - (mins * 60);

    if (expanded)
        if (days == 0)
            sprintf(nix.sys_uptime, "%.2u hours, %.2u mins, %.2u secs",
                hours, mins, secs
            );
        else if (hours == 0)
            sprintf(nix.sys_uptime, "%.2u mins, %.2u secs",
                mins, secs
            );
        else if (mins == 0)
            sprintf(nix.sys_uptime, "%.2u secs",
                secs
            );
        else
            sprintf(nix.sys_uptime, "%.2u days, %.2u hrs, %.2u mins, %.2u secs",
                days, hours, mins, secs
            );
    else
        if (days == 0)
            sprintf(nix.sys_uptime, "%.2uh, %.2um, %.2us",
                hours, mins, secs
            );
        else if (hours == 0)
            sprintf(nix.sys_uptime, "%.2um, %.2us",
                mins, secs
            );
        else if (mins == 0)
            sprintf(nix.sys_uptime, "%.2us",
                secs
            );
        else
            sprintf(nix.sys_uptime, "%.2ud, %.2uh, %.2um, %.2us",
                days, hours, mins, secs
            );
}

/**
 * Note: It's not necessary that every GNU/Linux distro will have a sugary name.
 * Chroot'd systems may or may not have "release" file. As such only rootfs systems
 * and also, some RHEL systems, seems to have "issue" or "version" file instead.
*/
static void get_os_release(void)
{
    char *release_file = "/etc/os-release";
    char *generic = "Generic GNU/Linux";
    long unsigned int pos;
    int ret;
    glob_t globbuf;

    ret = glob(release_file, GLOB_PERIOD, NULL, &globbuf);

    if (ret != 0 || globbuf.gl_pathc < 1) {
        sprintf(nix.distro_name, "%s", generic);
        goto end;
    }

    FILE *fp = fopen(globbuf.gl_pathv[0], "r");

    if (fp == NULL) {
        /** Assume we don't have the "release" file */
        sprintf(nix.distro_name, "%s", generic);
        fclose(fp);
        goto end;
    }

    fseek(fp, 0L, SEEK_END);
    pos = ftell(fp);
    rewind(fp);

    long unsigned int i, k;
    char txt[1024];

    memset(txt, '\0', sizeof(txt));

    fread(txt, 1, pos, fp);
    fclose(fp);
    globfree(&globbuf);

    for (i = 0; i < ARRAY_SIZE(txt); i++) {
        /* Avoid checking whole string of charecters */
        if (txt[i] == 'P' && txt[i + 6] == '_' && txt[i + 11] == '=') {
            for (k = 0; k < 13; k++)
                txt[i + k] = '\0';

            while (txt[i] != '"') {
                strncat(nix.distro_name, &txt[i], 1);
                i++;
            }
        }
    }

end:
    globfree(&globbuf);
    /* Reach here and continue */
}

/**
 * Get RAM information
*/
static void get_ram_info(void)
{
    struct sysinfo si;

    if (sysinfo(&si) != 0)
        error(EXIT_FAILURE, "sysinfo():");

    long unsigned int tiny_cache = ((si.bufferram * (long unsigned int)si.mem_unit) +
        (si.sharedram * (long unsigned int)si.mem_unit)) / 1024 / 1024;
    long unsigned int total = (si.totalram * (long unsigned int)si.mem_unit) / 1024 / 1024;

    /* 1024 * 1024 is equivalent to (10 << 20) - 9437184, if you want to do it with bit shift :) */
    sprintf(
        nix.ram_info, "%lu MiB/%lu MiB (%lu%%)",
        tiny_cache, total, ((tiny_cache * 100) / total)
    );
}

/**
 * Get current shell information (default shell)
 * Note: tty isn't a shell but it's just a last fallback.
*/
static void get_shell(void)
{
    char *sh = getenv("SHELL");

    if (sh != NULL) {
        if (strstr(sh, "/usr/bin") != NULL)
            sprintf(nix.shell_name, "%s", sh + 9);
        else if (strstr(sh, "/usr/local/bin") != NULL)
            sprintf(nix.shell_name, "%s", sh + 15);
        else if (strstr(sh, "/bin") != NULL)
            sprintf(nix.shell_name, "%s", sh + 5);
        else if (strstr(sh, "/sbin") != NULL)
            sprintf(nix.shell_name, "%s", sh + 6);
        else
            sprintf(nix.shell_name, "%s", sh);
    } else {
        struct passwd *pd;
        pd = getpwuid(getuid());
        sh = pd->pw_shell;

        if (pd == NULL)
            error(EXIT_FAILURE, "getpwuid():");

        if (sh != NULL) {
            if (strstr(pd->pw_shell, "/usr/bin") != NULL)
                sprintf(nix.shell_name, "%s", pd->pw_shell + 9);
            else if (strstr(sh, "/usr/local/bin") != NULL)
                sprintf(nix.shell_name, "%s", sh + 15);
            else if (strstr(sh, "/bin") != NULL)
                sprintf(nix.shell_name, "%s", sh + 5);
            else if (strstr(sh, "/sbin") != NULL)
                sprintf(nix.shell_name, "%s", sh + 6);
            else
                sprintf(nix.shell_name, "%s", sh);
        }
    }
}

/**
 * Get user information
*/
static void get_userinfo(void)
{
    struct passwd *pd;
    pd = getpwuid(getuid());

    if (pd == NULL)
        error(EXIT_FAILURE, "getpwuid():");

    sprintf(nix.user_name, "%s", pd->pw_name);
    nix.user_uid = pd->pw_uid;
}

/**
 * Get hostname, architecture, and kernel release
*/
static void get_uname(void)
{
    struct utsname ut;

    if (uname(&ut) != 0)
        error(EXIT_FAILURE, "uname():");

    sprintf(nix.host_name, "%s", ut.nodename);
    sprintf(nix.kern_arch, "%s", ut.machine);
    sprintf(nix.kern_release, "%s", ut.release);
}

/**
 * Help
*/
static void help(void)
{
    print(
        "nixfetch - v%s\nA little GNU/Linux system information retriever\n\n"
        "Commands\n"
        "   -h          - Show this help page\n"
        "   -a          - Output information with Linux logo (default)\n"
        "   -b          - Output CPU brand ID string\n"
        "   -c          - Set output color when information will be print (for logo and non-logo options)\n"
        "   -d          - Output information without the logo\n"
        "   -e          - Output CPU extended-family and extended-model ID\n"
        "   -g          - Output the iGPU name (Intel CPUs only)\n"
        "   -h          - Output this help information\n"
        "   -i          - Output current user UID\n"
        "   -l          - Output the CPU name\n"
        "   -u          - Output system uptime\n"
        "   -t          - Output the CPU type (Intel CPUs only)\n"
        "   -v          - Output nixfetch version\n"
        "   -w          - Set dd/mm/yyyy format to long\n"
        "   -x          - Output a or a list of CPU supported extensions (major ones only)\n"
        "   -y          - Output BIOS information\n\n"
        , VERSION
    );
}

/**
 * Linux fetch logo (default)
*/
static void print_info_w_logo(void)
{
    char *base_char = print_base(
        strlen(nix.user_name) + strlen(nix.host_name), "="
    );

    print(
        "         _nnnn_\n"
        "        dGGGGMMb                  %s@%s\n"
        "       @p~qp~~qMb                 %s\n"
        "       M|@||@) M|                 OS: %s\n"
        "       @,----.JM|                 Arch: %s\n"
        "      JS^\\__/  qKL                Kernel: %s\n"
        "     dZP        qKRb              Shell: %s\n"
        "    dZP          qKKb             UID: %d\n"
        "   fZP            SMMb            RAM: %s\n"
        "   HZM            MMMM            Loads: %s\n"
        "   FqM            MMMM            CPU: %s\n"
        " __| '.        |\\dS'qML           Type: %s\n"
        " |    '.       | \\Zq              Stepping: %d\n"
        " _)     \\.___.,|     .'           iGPU: %s\n"
        " \\____   )MMMMMP|   .'            Uptime: %s\n\n"
        , nix.user_name, nix.host_name, base_char
        , nix.distro_name, nix.kern_arch, nix.kern_release
        , nix.shell_name, nix.user_uid, nix.ram_info
        , nix.sys_loads, nix.label_str, nix.cpu_type
        , nix.stepping_id, nix.igpu_id, nix.sys_uptime
    );

    free(base_char);
}

/**
 * Without the logo
*/
static void print_info_wo_logo(void)
{
    print("OS: %s\nHost: %s\nArch: %s\nKernel: %s\nShell: %s\n"
        "User: %s\nUID: %d\nUptime: %s\nRAM: %s\nLoads: %s\n"
        "CPU: %s\niGPU: %s\n"
        , nix.distro_name, nix.host_name, nix.kern_arch
        , nix.kern_arch, nix.shell_name, nix.user_name
        , nix.user_uid, nix.sys_uptime, nix.ram_info
        , nix.sys_loads, nix.label_str, nix.igpu_id
    );
}

/**
 * Initialize these functions
*/
static void nix_call(int others, int w_uptime)
{
    get_os_release();
    get_uname();
    get_shell();
    get_userinfo();
    get_ram_info();
    get_sys_loads();
    get_cpu_label();
    get_cpu_type();
    get_cpu_extensions();
    get_stepping_id();
    get_igpu_info();
    get_bios_info();
    get_sys_uptime(w_uptime);

    if (others) {
        get_cpu_brand();
        get_extended_family();
        get_extended_model();
        get_cpu_cores();
    }
}

int main(int argc, char **argv)
{
    nix_call(0, 0);

    if (argc < 2) {
        print_info_w_logo();
        return EXIT_SUCCESS;
    }

    int idx = 0;

    while ((idx = getopt (argc, argv, "c:abdeghilutvwxy")) != -1) {
        switch (idx) {
        case 'a':
            print_info_w_logo();
            break;

        case 'b':
            print("CPU Brand: %s\n", nix.brand_str);
            if (strcmp(nix.igpu_vendor, "Unidentified") != 0)
                print("iGPU Brand: %s\n", nix.igpu_vendor);
            break;

        case 'c':
            pick_color = atoi(optarg);
            break;

        case 'd':
            print_info_wo_logo();
            break;

        case 'e':
            nix_call(1, 0);
            print(
                "Extended-Family: %d\n"
                "Extended-Model: %d\n"
                , nix.extended_family_id
                , nix.extended_model_id
            );
            break;

        case 'g':
            if (strcmp(nix.igpu_id, "Unknown") != 0)
                print("iGPU: %s\n", nix.igpu_id);
            else
                print("No iGPU specification found\n");
            break;

        case 'h':
            help();
            break;

        case 'i':
            print("UID: %d\n", nix.user_uid);
            break;

        case 'l':
            print("CPU: %s\n", nix.label_str);
            break;

        case 'u':
            print("Uptime: %s\n", nix.sys_uptime);
            break;

        case 't':
            if (strcmp(nix.cpu_type, "Unidentified") != 0) 
                print("CPU Type: %s\n", nix.cpu_type);
            else
                print("No manufacture signature was found\n");
            break;

        case 'v':
            print("nixfetch version: %s\n", VERSION);
            break;

        case 'w':
            nix_call(0, 1);
            break;

        case 'x':
            print("CPU-Extensions: %s\n", nix.cpu_extensions);
            break;

        case 'y':
            print(
                "BIOS Version: %s\n"
                "Manufacture Date: %s\n"
                "BIOS Vendor: %s\n"
                , nix.bios_version
                , nix.bios_date
                , nix.bios_vendor
            );
            break;

        default:
            break;
        }
    }

    for (; optind < argc; optind++) {
        print("%s: missing arguments after '%s'\n", argv[0], argv[optind]);
        exit(EXIT_FAILURE);
    }
}
