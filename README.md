## nixfetch
A little GNU/Linux system information retriever

This program is inspired from [bsdfetch](https://github.com/jhx0/bsdfetch)

Download binary build: [nixfetch.tar.gz](https://github.com/rilysh/nixfetch/files/10827889/nixfetch.tar.gz)

## Usage
You can also use `nixfetch -h` to get this information
```
nixfetch - v1.3-458a684
A little GNU/Linux system information retriever

Commands
   -h          - Show this help page
   -a          - Output information with Linux logo (default)
   -b          - Output CPU brand ID string
   -c          - Set output color when information will be print (for logo and non-logo options)
   -d          - Output information without the logo
   -e          - Output CPU extended-family and extended-model ID
   -g          - Output the iGPU name (Intel CPUs only)
   -h          - Output this help information
   -i          - Output current user UID
   -l          - Output the CPU name
   -u          - Output system uptime
   -t          - Output the CPU type (Intel CPUs only)
   -v          - Output nixfetch version
   -w          - Set dd/mm/yyyy format to long
   -x          - Output a or a list of CPU supported extensions (major ones only)
   -y          - Output BIOS information

```

## Notes
1. Since I used `cpuid` instruction which is only available in X86 and AMD64 architectures, you can't use nixfetch under a different architecture. (E.g. ARM, SPARC).
2. Function `get_cpu_type()` is only supported with Intel CPUs. OEM specifications might be different in AMD CPUs.
3. Function `get_cpu_cores()` is only supported with older Intel CPUs. Newer CPUs may have different approach. AMD has a different implementation for this.
4. There is no guarantee that function `get_os_release()` will return a GNU/Linux distribution name. Unlike other UNIX-like OS, GNU/Linux doesn't have a specified name, instead, it is just called "GNU/Linux" or "Linux" (in short). If there is no "release" file in `/etc/`, this function will return "Generic GNU/Linux" as the distribution name.
